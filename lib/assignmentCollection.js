Assignment = new Mongo.Collection('assignment');

AssignmentSchema = new SimpleSchema({
	"serialNumber": {
		type: String,
		label: "Serial Number",
		denyUpdate: true
	},
	"staffId": {
		type: String,
		label: "Staff Id"
	},
	"staffName": {
		type: String,
		label: "Staff Name"
	},
	"assignmentDate": {
		type: Date,
		label: "Date of Assignment",
	    denyUpdate: true,
		autoValue: function() {
		  if (this.isInsert) {
		    return new Date;
		  } else if (this.isUpsert) {
		    return {$setOnInsert: new Date};
		  } else {
		    this.unset();  // Prevent user from supplying their own value
		  }
		},
		autoform: {
			omit: true
		}
	},
	"locationId": {
		type: String,
		label: "Select Location",
		optional: true,
		autoform: {
			options: function() {
				var options = [];
				Locations.find().forEach(function (element) {
					options.push({
						label: element.locationName, value: element.locationId
					})
				});
				return options;
			}
		}
	}
})

Assignment.attachSchema(AssignmentSchema);


//Template.registerHelper("assignmentSchema", AssignmentSchema);