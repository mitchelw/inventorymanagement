Device = new Mongo.Collection('device');

var DeviceSchema = new SimpleSchema({
	make: {
		type: String
	},
	model: {
		type: String
	},
	serialNumber: {
		type: Number
	},
	deviceType: {
		type: String,
		autoform: {
			//omit: true
			optgroup: "Category",
			options: [
				{label: "Desktop", value: "Desktop"},
				{label: "Laptop", value: "Laptop"},
				{label: "Monitor", value: "Monitor"},
				{label: "Phone", value: "Phone"},
				{label: "Projector", value: "Projector"},
				{label: "Other", value: "Other"}
			]
		}
	},
	createdAt: {
	    type: Date,
	    denyUpdate: true,
		autoValue: function() {
		  if (this.isInsert) {
		    return new Date;
		  } else if (this.isUpsert) {
		    return {$setOnInsert: new Date};
		  } else {
		    this.unset();  // Prevent user from supplying their own value
		  }
		},
		autoform: {
			omit: true
		}
	},
	assigned: {
		type: Boolean,
		autoform: {
			omit: true
		},
		autoValue: function() {
			if (this.isInsert) {
				return false;
			};
		}
	}


});

Device.attachSchema(DeviceSchema);