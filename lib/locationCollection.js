Locations = new Mongo.Collection('locations');

AddressSchema = new SimpleSchema({
  street: {
    type: String,
    max: 100
  },
  city: {
    type: String,
    max: 50
  },
  state: {
    type: String,
    regEx: /^A[LKSZRAEP]|C[AOT]|D[EC]|F[LM]|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEHINOPST]|N[CDEHJMVY]|O[HKR]|P[ARW]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY]$/
  },
  zip: {
    type: String,
    regEx: /^[0-9]{5}$/
  }
});

LocationSchema = new SimpleSchema({
	locationId: {
		type: String,
		unique: true,
		denyUpdate: true
	},
	locationName: {
		type: String,
		unique: true,
		denyUpdate: true
	},
	address: {
		type: AddressSchema,
		minCount: 1
	}
});

Locations.attachSchema(LocationSchema);

//Template.registerHelper("addressSchema", AddressSchema);
