Router.configure({ layoutTemplate: 'layout'});
Router.route('/', {name: "landing"});


Router.route('/addInventory', {name: "addInventory"});
Router.route('/addLocation', {name: "addLocation"});
Router.route('/addAssignments', {name: "addAssignments"});


Router.route('/view', {name: "viewInventory"});
Router.route('/viewAssignments', {name: "viewAssignments"});
Router.route('/viewLocations', {name: "viewLocations"});

Router.route('/updateDevice', {name: "updateDevice"});
Router.route('/updateAssignment', {name: "updateAssignment"});

Router.route('/login', {name: "login"});
Router.route('/register', {name: "register"});