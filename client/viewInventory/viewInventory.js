Template.viewInventory.events({
	"click .delete": function (e) {
			var buttonId = e.currentTarget.id;
			// var serialId = Device.findOne({_id: buttonId});
			// console.log(serialId);
			// var assignedId = Assignment.find({"serialNumber": serialId.serialNumber});
			// console.log(assignedId);
			Device.remove(buttonId);
	},
	"click .update": function(e) {
		var buttonId = e.currentTarget.id;
		Session.set("updateId", buttonId);
		Router.go('/updateDevice');
	}

});

Template.viewInventory.helpers({
	device: function() {
		var deviceResults = [];

		Device.find().forEach(function(object){
			var commonInBoth = Assignment.findOne({"serialNumber": object.serialNumber});
			if (commonInBoth != null) {
				deviceResults.push([object, commonInBoth]);
				//console.log(commonInBoth);
				//console.log(object);
			} else {
				deviceResults.push(object);
			};
		});
		//console.log(deviceResults);
		return deviceResults;
		// var assignmentCursor = Assignment.find();
		// var deviceIds = assignmentCursor.map(function(p){return p.serialNumber});
		// console.log([assignmentCursor,
		// 		Device.find({_id: {$in: deviceIds}})]);
		// return [
		// 		assignmentCursor,
		// 		Device.find({_id: {$in: deviceIds}})];
		//return Device.find({}, {sort: {createdAt: -1}});
	},
	isAdmin: function() {
		if (Meteor.user().profile.position === "staff") {
			return false;
		} else {
			return true;
		};
	},
	isAssigned: function(assigned) {
		if (this.assigned === assigned) {
			return true;
		};
	},
	// assignedValue: function() {

	// }

});