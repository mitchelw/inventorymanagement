Template.layout.events({
	'click .logout': function(event){
		//event.preventDefalut();
		Meteor.logout();
		Router.go('landing');
	}
});

Template.nav.helpers({
	isAdmin: function() {
		if (Meteor.user().profile.position === "admin") {
			return true;
		} else {
			return false;
		};
	}
});