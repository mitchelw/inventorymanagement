Template.register.events({
	'submit #register-form' : function(e, t) {
			e.preventDefault();

			var options = {
				username: t.find('#account-username').value,
				password: t.find('#account-password').value,
				profile: {
					locationId : t.find('#locationId').value,
					fName: t.find('#fName').value,
					lName: t.find('#lName').value,
					position: "admin"
				}
			};

			Accounts.createUser(options, function(err){
					if (err) {
						// Inform the user that account creation failed
						console.log("Account Creation Failed");
					} else {
						// Success. Account has been created and the user
						// has logged in successfully. 
					}

				});

			Router.go("/view");
		}
});

Template.register.helpers({
	location: function() {
		return Locations.find();
	}
});