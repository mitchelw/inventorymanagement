Template.addAssignments.helpers({
	deviceData: function(){
		let availible = Device.find({assigned: false});
		return availible;
	},
	staffData: function() {
		let staff = Meteor.users.find({});
		return staff;
	}
});

Template.addAssignments.events({
	'submit #insertAssignmentForm' : function(e, t) {
		let locationReturn = Meteor.users.findOne({_id: t.find('#staff').value});
		let userName = locationReturn.profile.lName + ", " + locationReturn.profile.fName;

			e.preventDefault();
			let assignment = {
				serialNumber: t.find('#deviceType').value,
				staffId: t.find('#staff').value,
				staffName: userName,
				assignedDate: new Date(),
				locationId: locationReturn.profile.locationId
			}

			Assignment.insert(assignment);

			//replace = Device.findOne({serialNumber: t.find('#deviceType').value})._id;
			//Device.update({_id: replace}, {$set: {assigned: true}});
			Router.go('/view');
		}
});