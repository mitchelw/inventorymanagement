Template.updateDevice.events({
	'submit #updateDeviceForm' : function(e, t) {
			e.preventDefault();
			let deviceId = Session.get("updateId");
			Device.update(deviceId, {
				$set: {make: t.find('#make').value, model: t.find('#model').value, serialNumber: t.find('#serialNumber').value, deviceType: t.find('#deviceType').value}
			});
			Router.go('/view');
		}
});

Template.updateDevice.helpers({
	formData: function() {
		var deviceId = Session.get("updateId")
		return Device.findOne({_id: deviceId});
	}
});